// ******************************** after ********************************
/*
  after.h - Library for millisecond interval timing
  Created by Noel Dodd, March, 2019.
  
  Based on a blog comment by Julian Skidmore at:
  https://www.faludi.com/2007/12/18/arduino-millis-rollover-handling/

  Released into the public domain.
*/

#include <Arduino.h>

#define smillis() ((long)millis())

bool after(long timeout) {
  return smillis()-timeout>0;
}