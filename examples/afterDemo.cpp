// ******************************** after ********************************
/*
  Demo code for after.h - Library for millisecond interval timing
  Created by Noel Dodd, March, 2019.
  
  Based on a blog comment by Julian Skidmore at:
  https://www.faludi.com/2007/12/18/arduino-millis-rollover-handling/

  Released into the public domain.
*/

// Required for platformio 
#include <Arduino.h>

// include the library
#include "after.h"

// 'alarm' holds the alarm count 
long alarm = 0;          

// alarm interval of 1 second (1000 ms)
#define ALARM_INTV 1000

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);

  // Set up the first alarm interval to be 2 seconds (2000 ms)
  // smillis() is a macro defined in after.h
  alarm = smillis() + 2000;
}

void loop()
{
  if (after(alarm))
  // loop code can go here  

  // code here will run if the current milli counter is 'after' the alarm value
  {
    // reset the alarm for the next interval
    alarm = smillis() + ALARM_INTV;

    // do an arbitrary task once per alarm
    Serial.println("Testing...");
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

  // and loop code can go here

}