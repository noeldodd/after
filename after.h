// ******************************** after ********************************
/*
  after.h - Library for millisecond interval timing
  Created by Noel Dodd, March, 2019.
  
  Based on a blog comment by Julian Skidmore at:
  https://www.faludi.com/2007/12/18/arduino-millis-rollover-handling/

  Released into the public domain.
*/

#ifndef AFTER
#define AFTER

#include <Arduino.h>

#define smillis() ((long)millis())
//#define INTERVAL_SECOND 1000

bool after( long );

#endif